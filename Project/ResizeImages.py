from PIL import Image

def resizeImage(imageName):
    img = Image.open(imageName)
    img = img.resize((64,64), Image.ANTIALIAS)
    img.save(imageName)

for i in range(1, 1001):
    
    resizeImage("Dataset/0/zero_" + str(i) + '.jpg')
    resizeImage("Dataset/1/one_" + str(i) + '.jpg')
    resizeImage("Dataset/2/two_" + str(i) + '.jpg')
    resizeImage("Dataset/3/three_" + str(i) + '.jpg')
    resizeImage("Dataset/4/four_" + str(i) + '.jpg')
    resizeImage("Dataset/5/five_" + str(i) + '.jpg')
    
for i in range(1400, 1500):
    
    resizeImage("Dataset/0Test/zero_" + str(i) + '.jpg')
    resizeImage("Dataset/1Test/one_" + str(i) + '.jpg')
    resizeImage("Dataset/2Test/two_" + str(i) + '.jpg')
    resizeImage("Dataset/3Test/three_" + str(i) + '.jpg')
    resizeImage("Dataset/4Test/four_" + str(i) + '.jpg')
    resizeImage("Dataset/5Test/five_" + str(i) + '.jpg')
