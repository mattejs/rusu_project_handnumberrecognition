import tensorflow as tf
import tflearn
from tflearn.layers.conv import conv_2d,max_pool_2d
from tflearn.layers.core import input_data,dropout,fully_connected
from tflearn.layers.estimator import regression
import cv2
from sklearn.utils import shuffle


loadedImages = []
for i in range(1, 1001):
    image = cv2.imread('Dataset/0/zero_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    loadedImages.append(grayIimage.reshape(64, 64, 1))

for i in range(1, 1001):
    image = cv2.imread('Dataset/1/one_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    loadedImages.append(grayIimage.reshape(64, 64, 1))

for i in range(1, 1001):
    image = cv2.imread('Dataset/2/two_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    loadedImages.append(grayIimage.reshape(64, 64, 1))
    
for i in range(1, 1001):
    image = cv2.imread('Dataset/3/three_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    loadedImages.append(grayIimage.reshape(64, 64, 1))
    
for i in range(1, 1001):
    image = cv2.imread('Dataset/4/four_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    loadedImages.append(grayIimage.reshape(64, 64, 1))
    
for i in range(1, 1001):
    image = cv2.imread('Dataset/5/five_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    loadedImages.append(grayIimage.reshape(64, 64, 1))

outputVectors = []
for i in range(0, 1000):
    outputVectors.append([1, 0, 0, 0, 0, 0])

for i in range(0, 1000):
    outputVectors.append([0, 1, 0, 0, 0, 0])

for i in range(0, 1000):
    outputVectors.append([0, 0, 1, 0, 0, 0])
    
for i in range(0, 1000):
    outputVectors.append([0, 0, 0, 1, 0, 0])
    
for i in range(0, 1000):
    outputVectors.append([0, 0, 0, 0, 1, 0])
    
for i in range(0, 1000):
    outputVectors.append([0, 0, 0, 0, 0, 1])
    
    
testImages = []
for i in range(1400, 1500):
    image = cv2.imread('Dataset/0Test/zero_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    testImages.append(grayIimage.reshape(64, 64, 1))

for i in range(1400, 1500):
    image = cv2.imread('Dataset/1Test/one_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    testImages.append(grayIimage.reshape(64, 64, 1))

for i in range(1400, 1500):
    image = cv2.imread('Dataset/2Test/two_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    testImages.append(grayIimage.reshape(64, 64, 1))
    
for i in range(1400, 1500):
    image = cv2.imread('Dataset/3Test/three_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    testImages.append(grayIimage.reshape(64, 64, 1))
    
for i in range(1400, 1500):
    image = cv2.imread('Dataset/4Test/four_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    testImages.append(grayIimage.reshape(64, 64, 1))
    
for i in range(1400, 1500):
    image = cv2.imread('Dataset/5Test/five_' + str(i) + '.jpg')
    grayIimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    testImages.append(grayIimage.reshape(64, 64, 1))

testLabels = []
for i in range(0, 100):
    testLabels.append([1, 0, 0, 0, 0, 0])
    
for i in range(0, 100):
    testLabels.append([0, 1, 0, 0, 0, 0])

for i in range(0, 100):
    testLabels.append([0, 0, 1, 0, 0, 0])

for i in range(0, 100):
    testLabels.append([0, 0, 0, 1, 0, 0])

for i in range(0, 100):
    testLabels.append([0, 0, 0, 0, 1, 0])

for i in range(0, 100):
    testLabels.append([0, 0, 0, 0, 0, 1])

# Define the CNN Model
tf.compat.v1.reset_default_graph()
net = input_data(shape=[None,64,64,1],name='input')
net = conv_2d(net,32,2,activation='relu')
net = max_pool_2d(net,2)
net = conv_2d(net,64,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,128,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,256,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,256,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,128,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,64,2,activation='relu')
net = max_pool_2d(net,2)

net = fully_connected(net,1000,activation='relu')
net = dropout(net,0.75)

net = fully_connected(net,6,activation='softmax')

net = regression(net,optimizer='adam',learning_rate=0.001,loss='categorical_crossentropy',name='regression')

model=tflearn.DNN(net,tensorboard_verbose=0)

loadedImages, outputVectors = shuffle(loadedImages, outputVectors, random_state=0)

model.fit(loadedImages, outputVectors, n_epoch=100, validation_set = (testImages, testLabels), snapshot_epoch=True
          ,snapshot_step=200,show_metric=True, run_id='cnn')

model.save("TrainedModel/model.tfl")