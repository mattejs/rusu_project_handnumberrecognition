import tensorflow as tf
import tflearn
from tflearn.layers.conv import conv_2d,max_pool_2d
from tflearn.layers.core import input_data,dropout,fully_connected
from tflearn.layers.estimator import regression
import numpy as np
from PIL import Image
import cv2
import imutils

bg = None

def resizeImage(imageName):
    img = Image.open(imageName)
    img = img.resize((64,64), Image.ANTIALIAS)
    img.save(imageName)

def run_avg(image, aWeight):
    global bg
    if bg is None:
        bg = image.copy().astype("float")
        return

    cv2.accumulateWeighted(image, bg, aWeight)

def segment(image, threshold=25):
    global bg
    diff = cv2.absdiff(bg.astype("uint8"), image)

    thresholded = cv2.threshold(diff,
                                threshold,
                                255,
                                cv2.THRESH_BINARY)[1]

    (cnts, _) = cv2.findContours(thresholded.copy(),
                                    cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)

    if len(cnts) == 0:
        return
    else:
        segmented = max(cnts, key=cv2.contourArea)
        return (thresholded, segmented)

def main():
    aWeight = 0.5

    camera = cv2.VideoCapture(0)

    top, right, bottom, left = 20, 60, 225, 265

    num_frames = 0
    start_prediction = False

    while(True):
        (grabbed, frame) = camera.read()

        frame = imutils.resize(frame, width = 800)

        frame = cv2.flip(frame, 1)
        
        clone = frame.copy()

        (height, width) = frame.shape[:2]

        roi = frame[top:bottom, right:left]

        gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (7, 7), 0)

        if num_frames < 30:
            run_avg(gray, aWeight)
        else:
            hand = segment(gray)

            if hand is not None:
               
                (thresholded, segmented) = hand

                cv2.drawContours(clone, [segmented + (right, top)], -1, (0, 0, 255))
                if start_prediction:
                    cv2.imwrite('Image.jpg', thresholded)
                    resizeImage('Image.jpg')
                    predictedClass, confidence = getPredictedClass()
                    #print(predictedClass)
                    showPrediction(predictedClass, confidence)
                cv2.imshow("Thesholded", thresholded)

        cv2.rectangle(clone, (left, top), (right, bottom), (0,255,0), 2)

        num_frames += 1

        cv2.imshow("Video Feed", clone)

        keypress = cv2.waitKey(1) & 0xFF

        if keypress == ord("q"):
            break
        
        if keypress == ord("s"):
            start_prediction = True

def getPredictedClass():
    
    image = cv2.imread('Image.jpg')
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    prediction = model.predict([gray_image.reshape(64, 64, 1)])
    return np.argmax(prediction), (np.amax(prediction) / (prediction[0][0] + prediction[0][1] + prediction[0][2]))

def showPrediction(predictedClass, confidence):

    textImage = np.zeros((300,512,3), np.uint8)
    className = ""

    if predictedClass == 0:
        className = "Zero"
    elif predictedClass == 1:
        className = "One"
    elif predictedClass == 2:
        className = "Two"
    elif predictedClass == 3:
        className = "Three"
    elif predictedClass == 4:
        className = "Four"
    elif predictedClass == 5:
        className = "Five"

    cv2.putText(textImage,"Pedicted Class : " + className, 
    (30, 100), 
    cv2.FONT_HERSHEY_SIMPLEX, 
    1,
    (255, 255, 255),
    2)

    #cv2.putText(textImage,"Confidence : " + str(confidence * 100) + '%', 
    #(30, 100), 
    #cv2.FONT_HERSHEY_SIMPLEX, 
    #1,
    #(255, 255, 255),
    #2)
    cv2.imshow("Prediction", textImage)



tf.compat.v1.reset_default_graph()
net = input_data(shape=[None,64,64,1],name='input')
net = conv_2d(net,32,2,activation='relu')
net = max_pool_2d(net,2)
net = conv_2d(net,64,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,128,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,256,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,256,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,128,2,activation='relu')
net = max_pool_2d(net,2)

net = conv_2d(net,64,2,activation='relu')
net = max_pool_2d(net,2)

net = fully_connected(net,1000,activation='relu')
net = dropout(net,0.75)

net = fully_connected(net,6,activation='softmax')

net = regression(net,optimizer='adam',learning_rate=0.001,loss='categorical_crossentropy',name='regression')

model=tflearn.DNN(net,tensorboard_verbose=0)

model.load("TrainedModel/model.tfl")

main()
